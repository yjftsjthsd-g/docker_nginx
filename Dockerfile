FROM alpine:latest AS build

ARG VERSION=1.18.0

RUN apk add -U alpine-sdk coreutils pcre-dev zlib-dev

RUN ["/bin/mkdir", "/opt/nginx-build"]
WORKDIR /opt/nginx-build
RUN wget https://nginx.org/download/nginx-${VERSION}.tar.gz
RUN tar -x --strip-components=1 -f nginx-${VERSION}.tar.gz
RUN ./configure \
      --prefix=/opt/nginx \
      --error-log-path=/dev/stderr \
      --http-log-path=/dev/stdout
RUN make
RUN make install

FROM alpine:latest AS nginx
RUN apk add -U pcre zlib
COPY --from=build /opt/nginx /opt/nginx
EXPOSE 80
ENTRYPOINT ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]
