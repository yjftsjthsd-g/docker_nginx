# docker_nginx

Build and package nginx in a container.

The main purpose here is actually to let me test a build script that
self-updates to track the latest version available; I've done this before, but
not with container images.


## About the self-updating parts

The idea is this: Have a scheduled job run (here, daily) that can check what
version we're buiilding, and what the latest version is from upstream. If
they're different, make a new branch with our version bumped to match. Since we
also set up CI to run on branches, this results in us automatically attempting
to build and test the new version. As implemented in this repo, a passing CI
pipeline will result in the MR being merged back to the default branch.

Notable details:

* We set a variable, here `DO_A_BUMP=yes`, in the scheduled pipeline config;
  this lets us easily avoid running the version bump logic in regular pipelines.
  A more elegant solution doubtless exists, but I haven't figured it out.
* In this version, we avoid doing anything if there's already a branch for the
  new version.

### Setting up the self-updating parts

* Create a new SSH keypair
* Settings > Repository > Deploy keys: Fill in the public half of the SSH key
  and set "Grant write permissions to this key"
* Settings > CI/CD > Variables: Add the private half of the SSH key in a
  variable named `GIT_SSH_PRIV_KEY`; make sure to enable "Protected"
* Add the "bump" job to .gitlab-ci.yml (its current version should be
  repo-agnostic, although it may require tweaking ex. if you're on a private
  gitlab instance)
* CI/CD > Schedules > New schedule: Set the `DO_A_BUMP` variable to anything (I
  just use "1" or "yes"); set other values to taste


## TODOs 

* Figure out the layering situation; it's kinda nice that buildah will spit out
  a 1-layer image, but I feel like the Alpine base *should* be separate.
* Investigate fully static build of nginx; in theory, why should we even have an
  Alpine base in the final image?
* Try using `server_tokens off;` to disable sending the version to every client.
* According to https://linux-audit.com/hiding-nginx-version-number/ , it might
  be possible to avoid even telling clients that you're running nginx if you
  compile it yourself; I'm less convinced that this is useful, but it'd be
  interesting.
* Multiple build configs: reuse the same build logic with the same version
  number, but multiple Dockerfiles; this would let me have an alpine-based image
  AND a static binary image, AND a "paranoid" image that did things like
  removing nginx branding from headers.


## License

This repo is under the 2-clause BSD license (see LICENSE file), but note that
that only covers the contents of this repo (builds scripts, basically), not the
contents of the generated image! While nginx itself is also 2-clause BSD, the
Alpine Linux base image is not.

