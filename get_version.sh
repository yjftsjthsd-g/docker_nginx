#!/bin/sh

# So nginx publishes a "mainline" version, and a "stable" version; AFAICT,
# "mainline" means "beta" and "stable" means "stable", and I'm really hoping
# that the download page always lists exactly one "mainline" version so that
# this way of grabbing the *second* highest version number on that page does
# actually grab the "stable" version number.

curl -s https://nginx.org/en/download.html | grep -o 'nginx-[[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*\.tar.gz' | uniq | head -n 2 | tail -n 1 | grep -o '[[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*'

